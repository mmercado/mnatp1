\documentclass[a4paper]{article}

%% Sets page size and margins
\usepackage[margin=1.1in]{geometry}
\usepackage{setspace}
\spacing{1.5}

\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{cite}
\usepackage[colorlinks=true, allcolors=blue]{hyperref}
\usepackage{multicol}
\usepackage{graphicx}


\title{\textbf{Informe TP Nro.1 \\
Ising model \\}}

\date{30-09-2016}

\author{Marco Boschetti - 55266\\
Juan Cruz Lepore - 55124\\
Matías Mercado - 55019\\
Guido Matías Mogni - 55156\\
María Milagros Moreno Lafon - 55332 \\ \\}


\begin{document}
\maketitle

\pagenumbering{gobble}
\newpage
\pagenumbering{arabic}

\begin{center}
\section*{Resumen}
\end{center}

\paragraph{}
Este trabajo se aboca a la generación y la búsqueda de autovalores de matrices que sirven para el análisis del modelo de Ising, realizada de manera exacta y de manera aproximada.

La resolución exacta del modelo resulta de gran importancia en el ámbito de la Mecánica Estadística, y al ser exacta permite obtener resultados sin error. Por otro lado, la resolución aproximada es de utilidad para hallar autovalores de otras matrices diferentes a las utilizadas para analizar el modelo de Ising.

Por estos motivos, para el presente informe se implementaron y analizaron ambos tipos de metodologías.
Adicionalmente, se implementó un método para aproximar los autovalores reales de una matriz cualquiera, sobre el cual se realizaron distintas pruebas de benchmarking.

$\\$
$\\$
PALABRAS CLAVE: Modelo de Ising, Autovalores Complejos, Ortogonalidad, Hessenberg, Método QR.

\newpage
\pagenumbering{arabic}

\section{Introducción}
\paragraph{}
El modelo físico de Ising fue propuesto por el físico Wilhelm Lenz con el objetivo de estudiar el comportamiento de materiales ferromagnéticos. Lenz creó este modelo como un problema para uno de sus alumnos, Ernest Ising, para mostrar la presencia de un cambio de fase en el sistema. Ising demostró en su tesis en 1924 que en una dimensión no se produce este cambio de fase.

Una de las principales razones de la importancia del modelo estudiado es que se trata de uno de los pocos modelos de la Mecánica Estadística que posee solución analítica exacta \cite{IsingModel}.

El presente informe se divide en dos secciones principales: Metodología y Resultados y conclusiones. Las pruebas realizadas constan de tres etapas: la generación de la matriz de Ising, el desarrollo del método exacto y el desarrollo del aproximado. La explicación y justificación con los debidos modelos teóricos se encuentran en la sección de Metodología.

\newpage
\pagenumbering{arabic}

\section{Metodología}
\paragraph{}
Para el desarrollo de los problemas planteados, se implementaron algoritmos en Java 1.8. Todos los métodos implementados siguen el patrón de librerías estáticas, ya que son ejecutados en objetos que no tienen estado. Los códigos fuente de los algoritmos descritos a lo largo del informe se encuentran en el \href{https://bitbucket.org/mmercado/mnatp1/src}{repositorio de BitBucket} \cite{repository} que acompaña al mismo.

\subsection{Generación de matrices}
\paragraph{}
A lo largo de todo el trabajo se utilizan matrices generadas según la construcción descrita en Matrix Market \cite{MatrixMarket}. Para esto, se implementó un algoritmo que, a partir de los valores de $m$, $\alpha$ y $\beta$, construye una matriz Ising. Esta matriz está definida como:
\begin{align*}
  A=KL
\end{align*}

\paragraph*{}Donde las matrices $K$ y $L$ son matrices de dimensiones $2m$ x $2m$ de la forma:

\[
	K =
	\begin{pmatrix}
		E &  &  &  & \\
		& E &  &  & \\
		&  & \ddots &  & \\
		&  &  & E & \\
		&  &  &  & E
	\end{pmatrix}
\]

$\\$

\[
	E =
	\begin{pmatrix}
		cos(\alpha) & sin(\alpha)\\
		-sin(\alpha) & cos(\alpha)
	\end{pmatrix}
\]
$\\$
\[
	L =
	\begin{pmatrix}
		cos(\beta) &  &  &  & -sin(\beta)\\
		& F &  &  & \\
		&  & \ddots &  & \\
		&  &  & F & \\
		sin(\beta) &  &  &  &  cos(\beta)
	\end{pmatrix}
\]
$\\$
\[
	F =
	\begin{pmatrix}
		cos(\beta) & sin(\beta)\\
		-sin(\beta) & cos(\beta)
	\end{pmatrix}
\]

\paragraph*{}Para el modelado de cada matriz se utilizó un vector de vectores (nativos en Java), donde cada coeficiente tiene representación en punto flotante.

\subsection{Cálculo exacto de autovalores}
\paragraph{}
En la segunda consigna del trabajo se pide hallar los autovalores de la matriz A. Para ello se realizó el procedimiento sugerido en "Matrix Market". Este procedimiento permite encontrar los autovalores de A que es de 2m x 2m mediante el calculo de autovalores de matrices de 2x2.
Dicho procedimiento consiste en multiplicar las siguientes matrices:



\[
	X =
	\begin{pmatrix}
		cos(\alpha) & sin(\alpha)\\
		-sin(\alpha) & cos(\alpha)
	\end{pmatrix}
\]
\[
	Y_k =
	\begin{pmatrix}
		cos(\beta) & -\theta^k sin(\beta)\\
		\theta^{m-k} sin(\beta) & cos(\beta)
	\end{pmatrix}
\]


\begin{align*}
  \theta = e^\frac{i2\pi}{m} \\
  k : 1,2,...,m
\end{align*}

Se define:
\begin{align*}
  Z_k = XY_k
\end{align*}
\[
  Z_k =
	\begin{pmatrix}
		cos(\alpha)cos(\beta) + \theta^{m-k}sin(\alpha)sin(\beta) &
		-\theta^k cos(\alpha)sin(\beta) + sin(\alpha)cos(\beta)\\
		-sin(\alpha)cos(\beta) + \theta^{m-k}cos(\alpha)sin(\beta) &
		\theta^k sin(\alpha)sin(\beta) + cos(\alpha)cos(\beta)
	\end{pmatrix}
\]
$\\$
Luego se procede al cálculo de autovalores de la matriz $Z_k$. Para comodidad de los cálculos se redefine $Z_k$ como:

\[
	Z_k =
	\begin{pmatrix}
		a_1 & a_2\\
		a_3 & a_4
	\end{pmatrix}
\]
$\\$
Por lo tanto el determinante de $(Z_k -\lambda I)$ es:

\begin{align*}
  det(Z_k -\lambda I) = \lambda^2 - (a_1 + a_4) \lambda + a_1 a_4 - a_2 a_3
\end{align*}
$\\$
Se procede a calcular cada uno de los coeficientes del polinomio caraterístico.

\begin{align*}
a_1+a_4	= &  cos(\alpha)cos(\beta) + \theta^{m-k}sin(\alpha)sin(\beta) + \theta^k sin(\alpha)sin(\beta) + cos(\alpha)cos(\beta) \\
		= & 2cos(\alpha)cos(\beta) + sin(\alpha)sin(\beta) (\theta^{m-k} + \theta^k)
\end{align*}

\begin{align*}
a_1a_4	= &
	(cos(\alpha)cos(\beta) +
	\theta^{m-k}sin(\alpha)sin(\beta))
	(\theta^k sin(\alpha)sin(\beta) +
	cos(\alpha)cos(\beta)) \\
= & cos^2(\alpha)cos^2(\beta) +
	sin^2(\alpha)sin^2(\beta)\theta^m +
	cos(\alpha)sin(\alpha)cos(\beta)sin(\beta)\theta^k +
	cos(\alpha)sin(\alpha)cos(\beta)sin(\beta)\theta^{m-k} \\
=& 	cos^2(\alpha)cos^2(\beta) +
	sin^2(\alpha)sin^2(\beta)\theta^m +
	\frac{1}{4} . sin(2\alpha) sin (2\beta) (\theta^k + \theta^{m-k})
\end{align*}

\begin{align*}
	a_2a_3
	=& 	(-\theta^k cos(\alpha)sin(\beta) + sin(\alpha)cos(\beta))
		(-sin(\alpha)cos(\beta) + \theta^{m-k}cos(\alpha)sin(\beta)) \\
	=&	- sin^2(\alpha)cos^2(\beta) -
		cos^2(\alpha)sin^2(\beta)\theta^m +
		cos(\alpha)sin(\alpha)cos(\beta)sin(\beta)\theta^k +
		cos(\alpha)sin(\alpha)cos(\beta)sin(\beta)\theta^{m-k} \\
	=& - sin^2(\alpha)cos^2(\beta) -
		cos^2(\alpha)sin^2(\beta)\theta^m +
		\frac{1}{4} . sin(2\alpha) sin (2\beta) (\theta^k + \theta^{m-k})
\end{align*}
$\\$
Ahora se simplifican las expresiones de $\theta^m$ y $\theta^k + \theta^{m-k}$

\begin{align*}
	\theta^k + \theta^{m-k}
	=& 	(e^\frac{i2\pi}{m})^k + (e^\frac{i2\pi}{m})^{m-k} \\
	=& 	e^{i2\pi\frac{k}{m}} + e^{i2\pi(1-\frac{k}{m})} \\
	=&	cos(2\pi\frac{k}{m}) + isin(2\pi\frac{k}{m}) + cos(2\pi(1-\frac{k}{m})) + isin(2\pi(1-\frac{k}{m})) \\
	=&	cos(2\pi\frac{k}{m}) + cos(2\pi(1-\frac{k}{m})) + i(sin(2\pi\frac{k}{m}) + sin(2\pi(1-\frac{k}{m}))) \\
	=& cos(2\pi\frac{k}{m}) + cos(2\pi)cos(2\pi\frac{k}{m}) + sin(2\pi)sin(2\pi\frac{k}{m}) + i(sin(2\pi\frac{k}{m}) + sin(2\pi(1-\frac{k}{m}))) \\
	=& 2cos(2\pi\frac{k}{m}) + i(sin(2\pi\frac{k}{m}) + sin(2\pi)cos(2\pi\frac{k}{m}) - cos(2\pi)sin(2\pi\frac{k}{m})) \\
	=&	2cos(2\pi\frac{k}{m}) + i(sin(2\pi\frac{k}{m}) - sin(2\pi\frac{k}{m})) \\
	=& 2cos(2\pi\frac{k}{m})
\end{align*}

\begin{align*}
	\theta^m
	=	(e^\frac{i2\pi}{m})^m
	= 	e^{i2\pi}
	= 	cos(2\pi) + i sin(2\pi)
	= 1
\end{align*}

$\\$
Por esto, los coeficientes del polinomio característico son los siguientes:


\begin{align*}
	a_1+a_4
	=& 2cos(\alpha)cos(\beta)+2sin(\alpha)sin(\beta)cos(2\pi\frac{k}{m})
\end{align*}


\begin{align*}
	a_1a_4 -a_2a_3
	=& 	cos^2(\alpha)cos^2(\beta) +
		sin^2(\alpha)sin^2(\beta) +
		sin^2(\alpha)cos^2(\beta) +
		cos^2(\alpha)sin^2(\beta)
\end{align*}

$\\$
El polinomio obtenido se puede reescribir como:

\begin{align*}
	P(\lambda)
	=& 	\lambda^2 - tr(Z_k) \lambda + det(Z_k)
\end{align*}
$\\$
Por último, las dos raíces del $P(\lambda)$ son:

\begin{align*}
	\lambda_{1,2} = \frac{tr(Z_k)}{2} \pm \frac{\sqrt{tr(Z_k)^2 - 4 det(Z_k)}}{2}
\end{align*}

$\\$
\subsection{Cálculo aproximado de autovalores}
\paragraph{}
Con el fin de implementar un método que aproxime los autovalores de la matriz A, se exploraron diversos algoritmos.

En un primer momento, se consideró la implementación del método $QR$ aplicado sobre la matriz de Ising.
Una vez implementado el método, se observó que el comportamiento del mismo no era el deseado: en cada iteración la matriz A obtenida como $A=RQ$ era la misma matriz $A$ original. Analizando el procedimiento con más detalle, se concluyó que este comportamiento era debido a que la matriz $A$ de Ising es ortogonal, por lo que en la descomposición $QR$, resulta
$A = Q$ y $R = Id$, o bien los coeficientes de $Q$ y $R$ eran los mismos que los de $A$ y $Id$ respectivamente, con los signos opuestos.

Por otro lado, teniendo en cuenta la solución exacta de los autovalores descrita previamente en el presente informe,
se puede observar que el método $QR$ no puede converger a una matriz con autovalores en su diagonal principal, ya que todos ellos tienen una componente imaginaria no nula,
y la matriz producida por este método tiene todos sus coeficientes reales.

Como una alternativa, se optó por implementar una reducción de $A$ a la forma de Hessenberg utilizando transformaciones de Householder.

$
for$ $k \leftarrow 1 : n - 2\\
\indent\indent v \leftarrow H(k+1:n,k)\\
\indent\indent\alpha \leftarrow -norm(v)\\
\indent\indent if (v(1) < 0)\\
\indent\indent\indent\alpha \leftarrow -\alpha\\
\indent\indent end \\
\indent\indent v(1) \leftarrow v(1) - \alpha\\
\indent\indent v \leftarrow \frac{v}{norm(v)}\\
\indent\indent H(k+1:n,k+1:n) \leftarrow H(k+1:n,k+1:n) - 2 * v * (v^{T} * H(k+1:n,k+1:n))\\
\indent\indent H(k+1,k) \leftarrow \alpha\\
\indent\indent H(k+2:n,k) \leftarrow 0\\
\indent\indent H(1:n,k+1:n) \leftarrow H(1:n,k+1:n) - 2 * (H(1:n,k+1:n) * v) * v^{T} \\
\indent end\\
$


Esta transformación resulta efectiva ya que la matriz $H$, cuyos autovalores son los mismos que los de la matriz $A$ original,
no puede ser una matriz de Ising. Esto se debe a que la matriz $H$ obtenida es de Hessenberg, y en particular todos los elementos por debajo de la primera subdiagonal son ceros.
En particular, el elemento $A_{n,0}$ puede no ser nulo, mientras que el elemento $H_{n,0}$ necesariamente debe serlo.
Una vez implementado este método, se procedió a aplicarlo sobre la matriz de Ising para obtener una $H$ con los mismos autovalores.
Al aplicar el método $QR$ sobre $H$, se obtiene una matriz de Hessenberg que converge a sistemas de en su diagonal principal, cuyas raíces son los autovalores de $H$.

La dificultad de obtener estos autovalores radica en que no se sabe a priori la multiplicidad de cada autovalor, y por tanto se desconocen las dimensiones de cada sistema.

Esta limitación llevó a no poder desarrollar un algoritmo genérico en el cual se calculan los autovalores complejos de cualquier multiplicidad.

$\\$
\subsection{Cálculo de autovalores reales}
\paragraph{}
Adicionalmente se desarrolló una implementación del método iterativo $QR$, para hallar los autovalores reales de una matriz.

Se realizó una optimización a este método, haciendo uso de transformaciones de Householder implementadas anteriormente para obtener una matriz de Hessenberg.

De esta manera, se obtiene una matriz de Hessenberg a partir de la matriz original, y luego se realizan iteraciones del método $QR$.

Realizando pruebas de \textit{benchmarking} con distintas matrices se comprobó que el método $QR$ realizado a partir de una matriz de Hessenberg converge a los autovalores exactos con mayor rapidez que el método tradicional.

\newpage
\pagenumbering{arabic}

\section{Resultados y conclusiones}
\subsection{Cálculo de Autovalores para matrices ISIG}
\paragraph{}
Por los problemas analizados anteriormente con respecto a la multiplicidad de los autovalores, se decidió implementar una versión particular del algoritmo, que calcula los autovalores de una matriz ISIG donde todos sus autovalores son de multiplicidad dos.
A continuación se muestran ejemplos sobre distintas matrices:

Matriz $A$ generada con $m = 2, \alpha = \pi, \beta =\frac{ \pi }{8}$

\[
A=  \begin{bmatrix}-0.923880 & 0.000000 & 0.000000 & 0.382683\\0.000000 & -0.923880 & -0.382683 & 0.000000\\0.000000 & 0.382683 & -0.923880 & 0.000000\\-0.382683 & 0.000000 & 0.000000 & -0.923880 \end{bmatrix}
 \]
$\\$
Autovalores calculados mediante el método exacto:
\begin{itemize}
  \item -0.9238795325112867 + 0.38268343236508984 i
  \item -0.9238795325112867 - 0.38268343236508984 i
  \item -0.9238795325112867 + 0.38268343236508984 i
  \item -0.9238795325112867 - 0.38268343236508984 i
\end{itemize}
$\\$
Autovalores calculados mediante el método aproximado:
\begin{itemize}
\item -0.9238795325112941 + 0.38268343236509095 i
\item -0.9238795325112941 - 0.38268343236509095 i
\item -0.9238795325112862 + 0.3826834323650902 i
\item -0.9238795325112862 - 0.3826834323650902 i
\end{itemize}
$\\$
Matriz $A$ generada con $m = 2, \alpha = \pi, \beta =\frac{ \pi }{2}$
\[
 A=  \begin{bmatrix}0.000000 & 0.000000 & -0.963903 & 0.000000 & 0.000000 & 0.000000 & 0.000000 & 0.266255\\0.000000 & 0.000000 & -0.266255 & 0.000000 & 0.000000 & 0.000000 & 0.000000 & -0.963903\\0.000000 & 0.266255 & 0.000000 & 0.000000 & -0.963903 & 0.000000 & 0.000000 & 0.000000\\0.000000 & -0.963903 & 0.000000 & 0.000000 & -0.266255 & 0.000000 & 0.000000 & 0.000000\\0.000000 & 0.000000 & 0.000000 & 0.266255 & 0.000000 & 0.000000 & -0.963903 & 0.000000\\0.000000 & 0.000000 & 0.000000 & -0.963903 & 0.000000 & 0.000000 & -0.266255 & 0.000000\\-0.963903 & 0.000000 & 0.000000 & 0.000000 & 0.000000 & 0.266255 & 0.000000 & 0.000000\\-0.266255 & 0.000000 & 0.000000 & 0.000000 & 0.000000 & -0.963903 & 0.000000 & 0.000000 \end{bmatrix}
 \]

$\\$
Autovalores calculados mediante el método exacto:
\begin{itemize}
\item 0.0 + 1.0 i
\item 0.0 - 1.0 i
\item 0.0 + 1.0 i
\item 0.0 - 1.0 i
\item 0.0 + 1.0 i
\item 0.0 - 1.0 i
\item 0.0 + 1.0 i
\item 0.0 - 1.0 i
\end{itemize}
$\\$
Autovalores calculados mediante el método aproximado:
\begin{itemize}
\item 0.0 + 0.9999999999999999 i
\item 0.0 - 0.9999999999999999 i
\item 0.0 + 0.9999999999999999 i
\item 0.0 - 0.9999999999999999 i
\item 0.0 + 1.0000000000000002 i
\item 0.0 - 1.0000000000000002 i
\item 0.0 + 1.0000000000000002 i
\item 0.0 - 1.0000000000000002 i
\end{itemize}

\subsection{Cálculo de Autovalores Reales}
\paragraph{}
A continuación se presentan comparaciones entre el método QR tradicional y la optimización basada en la utilización de la matriz de Hessenberg a partir de transformaciones de Householder.

Cabe destacar que para asegurar que todas las matrices tengan todos sus autovalores reales, se utilizaron matrices simétricas, generadas aleatoriamente.


\begin{table}[]
\begin{tabular}{|c|l|l|l|l|}
\hline
\multicolumn{1}{|l|}{} & \multicolumn{1}{c|}{\textbf{Matriz de 20x20}} & \multicolumn{1}{c|}{\textbf{Matriz de 30x30}} & \multicolumn{1}{c|}{\textbf{Matriz de 40x40}} & \multicolumn{1}{c|}{\textbf{Matriz de 50x50}} \\ \hline
\textbf{\begin{tabular}[c]{@{}c@{}}Método \\ Iterativo QR\end{tabular}} & \begin{tabular}[c]{@{}l@{}}Tiempo: 2377\\ Iteraciones: 5509\end{tabular} & \begin{tabular}[c]{@{}l@{}}Tiempo: 1528\\ Iteraciones: 1153\end{tabular} & \begin{tabular}[c]{@{}l@{}}Tiempo: 3102\\ Iteraciones: 910\end{tabular} & \begin{tabular}[c]{@{}l@{}}Tiempo: 28988\\ Iteraciones: 4429\end{tabular} \\ \hline
\textbf{\begin{tabular}[c]{@{}c@{}}Variante con \\ Hessenberg\end{tabular}} & \begin{tabular}[c]{@{}l@{}}Overhead: 1\\ Tiempo: 2078\\ Iteraciones: 4804\end{tabular} & \begin{tabular}[c]{@{}l@{}}Overhead: 3\\ Tiempo: 996\\ Iteraciones: 754\end{tabular} & \begin{tabular}[c]{@{}l@{}}Overhead: 4\\ Tiempo: 2755\\ Iteraciones: 886\end{tabular} & \begin{tabular}[c]{@{}l@{}}Overhead: 7\\ Tiempo: 10866\\ Iteraciones: 1812\end{tabular} \\ \hline
\end{tabular}
\end{table}

\newpage
\pagenumbering{arabic}

$\\$
$\\$
\renewcommand{\refname}{Bibliografía}

\begin{thebibliography}{3}

\bibitem{IsingModel}
  Ising Model,
  https://en.wikipedia.org/wiki/Ising\_model.

\bibitem{MatrixMarket}
  Matrix Market,
  http://math.nist.gov/MatrixMarket/data/NEP/mvmisg/mvmisg.html.

\bibitem{repository}
  https://bitbucket.org/mmercado/mnatp1/src.

\end{thebibliography}

\end{document}