
public class QRmethod {

    /**
     *
     * @param A The matrix to obtain the eigenvalues
     * @param steps The times to iterate
     * @return
     */
    public static double[][] applyMethod(double[][] A, int steps){

        if(A.length != A[0].length){
            throw new IllegalArgumentException("A should be square! Sorry mate");
        }

        double[][] Aans = A.clone();
        double[][] Q = new double[A.length][A.length];
        double[][] R = new double[A.length][A.length];

        for(int i = 0; i < steps; i++){
            QRdecomposition.applyMethod(Aans,Q,R);
            Aans = MatrixGenerator.matrixProduct(R,Q);
        }

        return Aans;
    }

}
