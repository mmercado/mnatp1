import java.util.function.DoubleBinaryOperator;

public class MatrixGenerator {
    /**
     * Generates a MVMISG matrix with the given m, alpha, beta
     *
     * @param m     the generated matrix will have 2m x 2m size
     * @param alpha used for calculating matrix values
     * @param beta  used for calculating matrix values
     * @return the generated value
     */
    public static double[][] generateA(int m, double alpha, double beta) {
        double[][] matrixK = new double[2 * m][2 * m];

        for (int x = 0; x < m; x++) {
            matrixK[x * 2][x * 2] = Math.cos(alpha);
            matrixK[x * 2][x * 2 + 1] = Math.sin(alpha);
            matrixK[x * 2 + 1][x * 2] = -1 * Math.sin(alpha);
            matrixK[x * 2 + 1][x * 2 + 1] = Math.cos(alpha);
        }
        double[][] matrixL = new double[2 * m][2 * m];

        matrixL[0][0] = Math.cos(beta);
        matrixL[0][2 * m - 1] = -1 * Math.sin(beta);
        matrixL[2 * m - 1][0] = Math.sin(beta);
        matrixL[2 * m - 1][2 * m - 1] = Math.cos(beta);

        for (int x = 0; x < m - 1; x++) {
            matrixL[1 + x * 2][1 + x * 2] = Math.cos(beta);
            matrixL[1 + x * 2][1 + x * 2 + 1] = Math.sin(beta);
            matrixL[1 + x * 2 + 1][1 + x * 2] = -1 * Math.sin(beta);
            matrixL[1 + x * 2 + 1][1 + x * 2 + 1] = Math.cos(beta);
        }

        return truncateCero(matrixProduct(matrixK, matrixL));
    }

    public static double[][] matrixProduct(double[][] matrixK, double[][] matrixL) {
        if (matrixK[0].length != matrixL.length) {
            throw new IllegalArgumentException(String.format("Hey, columns in K(%dx%d) dont match with rows in L(%dx%d) =/",
                    matrixK.length, matrixK[0].length, matrixL.length, matrixL[0].length));
        }

        double[][] ans = new double[matrixK.length][matrixL[0].length];

        for (int i = 0; i < matrixK.length; i++) {
            for (int j = 0; j < matrixL[0].length; j++) {
                for (int index = 0; index < matrixK[0].length; index++) {
                    ans[i][j] += matrixK[i][index] * matrixL[index][j];
                }
            }
        }
        return ans;
    }


    public static void printMatrix(double[][] matrix) {
        for (int j = 0; j < matrix[0].length; j++) {
            System.out.print("------");
        }
        System.out.println();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(String.format("%s%.2f|", matrix[i][j] >= 0 ? " " : "", matrix[i][j]));
            }
            System.out.println();
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print("------");
            }
            System.out.println();
        }
        System.out.println();
    }


    public static double[][] getRandomMatriz(int rows, int cols) {
        double[][] random = new double[rows][cols];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                random[i][j] = Math.floor(Math.random() * 100) / 100;
            }
        }

        return random;
    }

    public static double[][] getIdMatrix(int size) {
        double[][] idMat = new double[size][size];
        for (int i = 0; i < size; i++) {
            idMat[i][i] = 1;
        }
        return idMat;
    }

    public static double[][] matrixOperation(final double[][] a, final double[][] b, final DoubleBinaryOperator op) {
        double[][] ans = new double[a.length][a[0].length];

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                ans[i][j] = op.applyAsDouble(a[i][j], b[i][j]);
            }
        }
        return ans;
    }

    public static double[][] matrixOperation(final double[][] a, final double b, final DoubleBinaryOperator op) {
        double[][] ans = new double[a.length][a[0].length];

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                ans[i][j] = op.applyAsDouble(a[i][j], b);
            }
        }
        return ans;
    }

    public static double norma2(double[] a) {
        double ans = 0;

        for (int i = 0; i < a.length; i++) {
            ans += a[i] * a[i];
        }
        return Math.pow(ans, 0.5);
    }

    public static double innerProd(double[] a, double[] b) {
        double ans = 0;

        for (int i = 0; i < a.length; i++) {
            ans += a[i] * b[i];
        }
        return ans;
    }

    public static double[] arrayOperation(final double[] a, final double[] b, final DoubleBinaryOperator op) {
        double[] ans = new double[a.length];

        for (int i = 0; i < a.length; i++) {
            ans[i] = op.applyAsDouble(a[i], b[i]);
        }
        return ans;
    }

    public static double[] arrayOperation(final double[] a, final double b, final DoubleBinaryOperator op) {
        double[] ans = new double[a.length];

        for (int i = 0; i < a.length; i++) {
            ans[i] = op.applyAsDouble(a[i], b);
        }
        return ans;
    }

    public static double[][] getColumn(double[][] matrix, int column) {
        double[][] ans = new double[matrix.length][1];
        for (int i = 0; i < matrix.length; i++) {
            ans[i][0] = matrix[i][column];
        }
        return ans;
    }

    public static double[][] tranpose(double[][] rowI) {
        double[][] ans = new double[rowI[0].length][rowI.length];
        for (int i = 0; i < rowI.length; i++) {
            for (int j = 0; j < rowI[0].length; j++) {
                ans[j][i] = rowI[i][j];
            }
        }
        return ans;
    }

    public static double[][] tranpose(double[] rowI) {
        double[][] ans = new double[rowI.length][1];
        for (int i = 0; i < rowI.length; i++) {
            ans[i][0] = rowI[i];
        }
        return ans;
    }

    public static double[][] submatrix(double[][] q, int x0, int x1, int y0, int y1) {
        if (x0 < 0 || y0 < 0 || x1 >= q.length || y1 >= q[0].length) {
            throw new IllegalStateException("Hey! Check those dimensions =D");
        }

        double[][] ans = new double[1 + x1 - x0][1 + y1 - y0];

        for (int i = x0; i <= x1; i++) {
            for (int j = y0; j <= y1; j++) {
                ans[i - x0][j - y0] = q[i][j];
            }
        }
        return ans;
    }

    private final static double EPSILON = Math.pow(10, -12);

    public static boolean compare(double[][] a, double[][] b) {
        if (a.length != b.length || a[0].length != b[0].length) {
            return false;
        }
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                if (Math.abs(a[i][j] - b[i][j]) > EPSILON) {
                    System.out.println(a[i][j] + "!=" + b[i][j]);
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean isCero(double[][] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                if (Math.abs(a[i][j]) > EPSILON) {
                    return false;
                }
            }
        }
        return true;
    }

    public static double[][] truncateCero(double[][] q) {
        for (int i = 0; i < q.length; i++) {
            for (int j = 0; j < q[0].length; j++) {
                if (Math.abs(q[i][j]) < EPSILON) {
                    q[i][j] = 0;
                }
            }
        }
        return q;
    }

    public static void printWolfram(double[][] A) {
        System.out.print("{");
        for (int i = 0; i < A.length; i++) {
            System.out.print("{");
            for (int j = 0; j < A[0].length; j++) {
                System.out.print(String.format("%.4f", A[i][j]));
                if (j < A[0].length - 1) {
                    System.out.print(",");
                }
            }
            System.out.print("}");
            if (i < A.length - 1) {
                System.out.print(",");
            }
        }
        System.out.print("}");
        System.out.println();
    }

    public static void printOctave(double[][] A) {
        System.out.print("[");
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A[0].length; j++) {
                System.out.print(String.format("%f", A[i][j]));
                if (j < A[0].length - 1) {
                    System.out.print(",");
                }
            }
            if (i < A.length - 1) {
                System.out.print(";");
            }
        }
        System.out.print("]");
        System.out.println();
    }

    public static void printLatex(double[][] A) {
        System.out.print("\\[ \n M=  \\begin{bmatrix}");
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A[0].length; j++) {
                System.out.print(String.format("%f", A[i][j]));
                if (j < A[0].length - 1) {
                    System.out.print("\\\\");
                }
            }
            if (i < A.length - 1) {
                System.out.print(";");
            }
        }
        System.out.print(" \\end{bmatrix}\n \\]");
        System.out.println();
    }

    public static double[] getDiagonal(double[][] qra) {
        double[] ans = new double[qra.length];
        for (int i = 0; i < qra.length; i++) {
            ans[i] = qra[i][i];
        }
        return ans;
    }

    public static double[][] getSimetricMatrix(int rows, int cols) {

        double[][] random = new double[rows][cols];
        for (int i = 0; i < rows; i++) {
            for (int j = i; j < cols; j++) {
                double rand = Math.floor(Math.random() * 100) / 100;
                random[i][j] = rand;
                random[j][i] = rand;
            }
        }
        return random;
    }

    public static double[][] clone(double[][] A){
        double[][] ans = new double[A.length][A[0].length];
        for(int i = 0; i < A.length; i++){
            for(int j = 0; j < A[0].length; j++){
                ans[i][j] = A[i][j];
            }
        }
        return ans;
    }
}
