import java.util.ArrayList;
import java.util.List;

public class Eigenvalues {
    private double alpha, beta, m;
    private final double EPSILON = Math.pow(10, -9);

    public Eigenvalues(double m, double alpha, double beta) {
        this.alpha = alpha;
        this.beta = beta;
        this.m = m;
    }


    public double getAlpha() {
        return alpha;
    }

    public void setAlpha(double alpha) {
        this.alpha = alpha;
    }

    public double getBeta() {
        return beta;
    }

    public void setBeta(double beta) {
        this.beta = beta;
    }

    public double getM() {
        return m;
    }

    public void setM(double m) {
        this.m = m;
    }

    /**
     * Calculates the exact value of all the eigenvalues for the given matrix, by calculating each
     * 2x2 submatrix's eigenvalues.
     * @return the matrix's eigenvalues
     */
    public List<Complex> getEngineValues() {
        double cosSquareA = Math.cos(this.alpha) * Math.cos(this.alpha);
        double sinSquareA = Math.sin(this.alpha) * Math.sin(this.alpha);
        double cosSquareB = Math.cos(this.beta) * Math.cos(this.beta);
        double sinSquareB = Math.sin(this.beta) * Math.sin(this.beta);

        double cosA = Math.cos(this.alpha);
        double cosB = Math.cos(this.beta);
        double sinA = Math.sin(this.alpha);
        double sinB = Math.sin(this.beta);

        double sin2A = Math.sin(2*this.alpha);
        double sin2B = Math.sin(2*this.beta);

        List<Complex> list =  new ArrayList<Complex>();

        for(int k = 1; k <= m; k++) {
            double coskm = Math.cos(2 * Math.PI * k / this.m);
            double b = cosA * cosB * 2 + sinA * sinB * coskm * 2;
            double a = 1;
            double c = cosSquareA * cosSquareB + sinSquareA * sinSquareB + sinSquareA * cosSquareB + sinSquareB * cosSquareA;

            if(b*b < 4 * c) {
                double real = (Math.abs(b/2) < EPSILON)? 0 : b/2;
                double img = Math.sqrt(4 * c - b * b) / 2;

                Complex r1 = new Complex(real, img);
                Complex r2 = new Complex(real, - img);


                list.add(r1);
                list.add(r2);

            } else {

                double ra = (Math.abs(b/2) < EPSILON)? 0 : b/2 - Math.sqrt(b * b - 4 * c);
                double rb = (Math.abs(b/2) < EPSILON)? 0 : b/2 + Math.sqrt(b * b - 4 * c);

                Complex r1 = new Complex(ra, 0);
                Complex r2 = new Complex(rb, 0);

                list.add(r1);
                list.add(r2);

            }

        }
        return list;
    }
}
