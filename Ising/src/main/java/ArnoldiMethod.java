/**
 * Created by tritoon on 27/09/16.
 */
public class ArnoldiMethod {

    public static void applyMethod(final double[][] A, final double[][] q1,double[][]Q, double[][] H) {

        if(A.length != A[0].length){
            throw new IllegalArgumentException("A should be square! Sorry mate");
        }

        if(q1.length != A.length || q1[0].length!=1){
            throw new IllegalArgumentException("q1 should be a column vector! Check that");
        }

        int n = A.length;

        for(int c = 0; c < q1.length; c++){
            Q[c][0] = q1[c][0];
        }

        for(int i = 1; i < n; i++){
            double[][] colI = ortogonalize(MatrixGenerator.matrixProduct(A,MatrixGenerator.getColumn(A,i-1)),Q,i);

            while(MatrixGenerator.isCero(colI)){
                colI = ortogonalize(MatrixGenerator.getRandomMatriz(colI.length,colI[0].length),Q,i);
            }

            for(int c = 0; c < colI.length; c++){
                Q[c][i] = colI[c][0];
            }
        }

        double[][] Qtrans = MatrixGenerator.tranpose(Q);
        double[][] Rans = MatrixGenerator.matrixProduct(Qtrans,A);

        for(int i = 0; i < Rans.length; i++){
            for(int j = 0; j < Rans.length; j++){
                H[i][j] = Rans[i][j];
            }
        }

        if(!MatrixGenerator.compare(A,MatrixGenerator.matrixProduct(Q,H))){
            //      throw new IllegalStateException("QR decomposition not possible. Probably a non LI initial matrix. Sorry =/");
        }

    }


    /**
     *
     * @param ansColumn The column vector to make ortonormal to the other vectors
     * @param Q with the ortonormal vectors in the leftmost columns
     * @param ortogonals the number of orthogonal columns
     * @return
     */
    private static double[][] ortogonalize(double[][] ansColumn, double[][] Q, int ortogonals) {

        double[][] Pi = ansColumn.clone();
        for(int i = 0; i < ortogonals; i++){
            double[] columnI = MatrixGenerator.tranpose(MatrixGenerator.getColumn(Q,i))[0];

            double innerProd =  MatrixGenerator.innerProd(columnI,MatrixGenerator.tranpose(ansColumn)[0]);
            double[] innerProdProdCol = MatrixGenerator.arrayOperation(columnI,innerProd,(x,y)-> x * y);
            double[][] columnICol = MatrixGenerator.tranpose(innerProdProdCol);
            Pi = MatrixGenerator.matrixOperation(Pi,columnICol,(x,y)-> x - y);
        }

        double normaPi = MatrixGenerator.norma2(MatrixGenerator.tranpose(Pi)[0]);
        double[][] ans = MatrixGenerator.matrixOperation(Pi,normaPi,(x,y)-> x/y);
        return ans;
    }
}
