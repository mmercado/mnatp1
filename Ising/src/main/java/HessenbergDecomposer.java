/**
 * Created by tritoon on 18/09/16.
 */
public class HessenbergDecomposer {

    public static double[][] applyMethod(double[][] Q) {
        int n = Q.length;
        for (int k = 0; k < n - 2; k++) {

            double[][] v = MatrixGenerator.submatrix(Q, k + 1, n - 1, k, k);
            double norma2V = MatrixGenerator.norma2(MatrixGenerator.tranpose(v)[0]);
            double alpha = -1 * norma2V;

            if (v[0][0] < 0) {
                alpha *= -1;
            }

            v[0][0] -= alpha;
            norma2V = MatrixGenerator.norma2(MatrixGenerator.tranpose(v)[0]);
            v = MatrixGenerator.matrixOperation(v, norma2V, (x, y) -> x / y);


            double[][] vtxQ = MatrixGenerator.matrixProduct(MatrixGenerator.tranpose(v),
                    MatrixGenerator.submatrix(Q, k + 1, n - 1, k + 1, n - 1));
            double[][] vvtxQ = MatrixGenerator.matrixProduct(v, vtxQ);

            for (int i = k + 1; i < n; i++) {
                for (int j = k + 1; j < n; j++) {
                    Q[i][j] -= 2 * vvtxQ[i-k-1][j-k-1];
                }
            }

            Q[k + 1][k] = alpha;
            for (int i = k + 2; i < n; i++) {
                Q[i][k] = 0;
            }

            double[][] Qxv = MatrixGenerator.matrixProduct(MatrixGenerator.submatrix(Q, 0, n - 1, k + 1, n - 1), v);
            double[][] Qxvxvt = MatrixGenerator.matrixProduct(Qxv, MatrixGenerator.tranpose(v));

            for (int i = 0; i < n; i++) {
                for (int j = k + 1; j < n; j++) {
                    Q[i][j] -= 2 * Qxvxvt[i][j-k-1];
                }
            }
        }
        return Q;
    }
}