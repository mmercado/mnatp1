public class Main {

    public static void main(String[] args) {


        compareQRMethod();

    }

    private static void compareQRMethod() {
        double[][] A = MatrixGenerator.getSimetricMatrix(8,8);

        System.out.println("'A':\n");
        MatrixGenerator.printOctave(A);

        double[] exactSolutions = MatrixGenerator.getDiagonal( QRmethod.applyMethod(A,99999));


        int noHessembergIterations = 0;
        double[][] QRA = MatrixGenerator.clone(A);
        double[] noHessemberg = MatrixGenerator.getDiagonal(QRA);
        while(!match(noHessemberg,exactSolutions)) {
            noHessembergIterations++;
            QRA = QRmethod.applyMethod(QRA,1);
            noHessemberg = MatrixGenerator.getDiagonal(QRA);
        }

        System.out.println("QR no Hessemberg METHOD: "+noHessembergIterations);

        double[][] H = HessenbergDecomposer.applyMethod(A);
        int hessembergIterations = 0;
        double[][] QRH =  MatrixGenerator.clone(H);
        double[] hessemberg = MatrixGenerator.getDiagonal(QRH);

        while(!match(hessemberg,exactSolutions)) {
            hessembergIterations++;
            QRH =  QRmethod.applyMethod(QRH,1);
            hessemberg = MatrixGenerator.getDiagonal(QRH);
        }

        System.out.println("QR Hessemberg METHOD: "+hessembergIterations);

    }

    private static final double delta = 0.0001;
    private static boolean match(double[] noHessemberg, double[] exactSolutions) {
        for(int i = 0; i < noHessemberg.length; i++){
            if(Math.abs(noHessemberg[i] - exactSolutions[i]) > delta){
                return false;
            }
        }
        return true;
    }
}
