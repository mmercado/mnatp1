public class QRdecomposition {

    /**
     *
     * @param A The original matrix, to be decomposed into Q*R
     * @param Q A matrix to leave the result of the method
     * @param R A matrix to leave the result of the method
     */
    public static void applyMethod(final double[][] A, double[][]Q, double[][] R) {

        if(A.length != A[0].length){
            throw new IllegalArgumentException("A should be square! Sorry mate");
        }

        int n = A.length;
        for(int i = 0; i < n; i++){
            double[][] colI = gramSchmidt(MatrixGenerator.getColumn(A,i),Q,i);

            while(MatrixGenerator.isCero(colI)){
                colI = gramSchmidt(MatrixGenerator.getRandomMatriz(colI.length,colI[0].length),Q,i);
            }

            for(int c = 0; c < colI.length; c++){
                Q[c][i] = colI[c][0];
            }
        }

        double[][] Qtrans = MatrixGenerator.tranpose(Q);
        double[][] Rans = MatrixGenerator.matrixProduct(Qtrans,A);

        for(int i = 0; i < Rans.length; i++){
            for(int j = 0; j < Rans.length; j++){
                R[i][j] = Rans[i][j];
            }
        }

        if(!MatrixGenerator.compare(A,MatrixGenerator.matrixProduct(Q,R))){
      //      throw new IllegalStateException("QR decomposition not possible. Probably a non LI initial matrix. Sorry =/");
        }

    }


    /**
     *
     * @param ansColumn The column vector to make ortonormal to the other vectors
     * @param Q with the ortonormal vectors in the leftmost columns
     * @param ortogonals the number of orthogonal columns
     * @return
     */
    private static double[][] gramSchmidt(double[][] ansColumn, double[][] Q, int ortogonals) {

        double[][] Pi = ansColumn.clone();
        for(int i = 0; i < ortogonals; i++){
            double[] columnI = MatrixGenerator.tranpose(MatrixGenerator.getColumn(Q,i))[0];

            double innerProd =  MatrixGenerator.innerProd(columnI,MatrixGenerator.tranpose(ansColumn)[0]);
            double[] innerProdProdCol = MatrixGenerator.arrayOperation(columnI,innerProd,(x,y)-> x * y);
            double[][] columnICol = MatrixGenerator.tranpose(innerProdProdCol);
            Pi = MatrixGenerator.matrixOperation(Pi,columnICol,(x,y)-> x - y);
        }

        double normaPi = MatrixGenerator.norma2(MatrixGenerator.tranpose(Pi)[0]);
        double[][] ans = MatrixGenerator.matrixOperation(Pi,normaPi,(x,y)-> x/y);
        return ans;
    }

    /** Gets the eigenvalues for the given matrix m.
     * The matrix must be square, with even dimensions, and all it's eigenvalues MUST be complex.
     * @param m the given matrix
     * @return the matrix eigenvalues
     */
    public static Complex[] getEigenvalues(double [][] m){
        final int rows=m.length;
        Complex[] eigenvalues = new Complex[rows];

        for(int i=0; i<=rows-2; i+=2){
            Complex[] roots = getRoots(m[i][i], m[i][i+1], m[i+1][i], m[i+1][i+1]);
            eigenvalues[i] = roots[0];
            eigenvalues[i+1] = roots[1];
        }
        return eigenvalues;
    }

    /**
     * Calculates the eigenvalues of a 2x2 matrix, by finding the characteristic polynomial roots
     * @param m1 the matrix element corresponding to position [0][0]
     * @param m2 the matrix element corresponding to position [0][1]
     * @param m3 the matrix element corresponding to position [1][0]
     * @param m4 the matrix element corresponding to position [1][1]
     * @return an array containing the eigenvalues.
     */
    public static Complex[] getRoots(final double m1, final double m2, final double m3, final double m4){
        Complex[] roots = new Complex[2];
        double a, b, c;
        a = 1;
        b = -1 * (m1 + m4);
        c = (m1*m4) - (m2*m3);

        double discriminant = b * b - 4 * a * c;
        if(discriminant < 0){
            double real = (-1 * b) / (2 * a);
            double img = Math.sqrt(-1 * discriminant) / (2 * a);
            Complex r1 = new Complex(real, img);
            Complex r2 = new Complex(real, -1* img);
            roots[0] = r1;
            roots[1] = r2;
        } else{
            double real1 = ( (-1 * b) + Math.sqrt(discriminant)) / (2 * a);
            double real2 = ( (-1 * b) - Math.sqrt(discriminant)) / (2 * a);
            double img =  0;
            Complex r1 = new Complex(real1, img);
            Complex r2 = new Complex(real2, img);
            roots[0] = r1;
            roots[1] = r2;
        }

        return roots;
    }
}
